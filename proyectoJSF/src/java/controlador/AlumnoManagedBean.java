
package controlador;

import entidades.Alumno;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import modelos.alumnoModel;

/**
 *
 * @author Fredy diaz
 */
@Named(value = "Balumno")
@SessionScoped
public class AlumnoManagedBean implements Serializable {

   
      private alumnoModel alumnoModel = new alumnoModel();
      private Alumno persona = new Alumno();
      
        private List<Alumno> listado = new ArrayList<>();
   
      public List<Alumno> getListado() {
        return listado;
    }
    
    public String agregar(){
        listado.add(this.persona);
        this.persona= new Alumno();
        
        return "index?faces-redirect=true";
    }
    
    public AlumnoManagedBean() {
    }
    
    
  public Alumno getalumnoModel() {
        return alumnoModel.Consultar();
    }
  
     public void setalumnoModel(alumnoModel alumnoModel) {
        this.alumnoModel = alumnoModel;
    }
      public Alumno getAlumno() {
        return persona;
    }

    public void setPersona(Alumno alumno) {
        this.persona = alumno;
    }
    
    
}
