
package entidades;

/**
 *
 * @author Fredy diaz
 */
public class Alumno {
    
   private int id; 
   private String nombre;
   private String apellido;
   private int edad;
   private int telefono;
   
   //Constructores mínimos 
   
   //vacio
   //todos los atributos menos el id
   //todos los atributos

    public Alumno() {
    }

    public Alumno(String nombre, String apellido, int edad, int telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.telefono = telefono;
    }

    public Alumno(int id, String nombre, String apellido, int edad, int telefono) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.telefono = telefono;
    }
    
   //set y get

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    
   
   
}
